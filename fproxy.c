#define _GNU_SOURCE
#define _FILE_OFFSET_BITS 64
#include <sys/types.h>
#include <sys/stat.h>
#include <sysexits.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>

#define eprintf(args...) fprintf(stderr, args)

#define TRANSFER_LEN (1024 * 128)
#define FD_IS_PIPE 0
#define FD_NOT_PIPE 1

// Redirect the file write's to the standard output/error stream.
#define OUTPUT_MODE 0
// Redirect the writes to the standard input to the file.
#define INPUT_MODE 1
#define BUF_SIZE_DEFAULT 0

#define MAX_PIPE_SIZE_SYS_PATH "/proc/sys/fs/pipe-max-size"
#define MAX_PIPE_SIZE_MAX_FILE_LEN 512

static sig_atomic_t should_exit = 0;

struct conf {
    int std_fd;
    int mode;
    int target_buf_size;
    int help;
    char* path;
};

/*
 * Simple wrapper for the open() syscall
 */
int open_w(const char* path, int flags) {
    int ret;
    do {
        ret = open(path, flags);
    } while (ret == -1 && errno == EINTR);
    return ret;
}

ssize_t read_w(int fd, char* buf, size_t buf_size) {
    int ret;
    do {
        ret = read(fd, buf, buf_size);
    } while (ret == -1 && errno == EINTR);
    return ret;
}

int get_conf(struct conf* conf, int argc, char** argv) {
    conf->std_fd = STDOUT_FILENO;
    conf->mode = OUTPUT_MODE;
    conf->target_buf_size = BUF_SIZE_DEFAULT;
    conf->help = 0;
    conf->path = NULL;
    int errored = 0;

    opterr = 1;
    while (1) {
        int opt = getopt(argc, argv, "eoib:h");
        if (opt == -1) {
            break;
        }
        switch (opt) {
            case 'e':
                conf->std_fd = STDERR_FILENO;
                conf->mode = OUTPUT_MODE;
                break;
            case 'o':
                conf->std_fd = STDOUT_FILENO;
                conf->mode = OUTPUT_MODE;
                break;
            case 'i':
                conf->std_fd = STDIN_FILENO;
                conf->mode = INPUT_MODE;
                break;
            case 'h':
                conf->help = 1;
                break;
            case 'b':
                int value = atoi(optarg);
                if (value == 0) {
                    errored = 1;
                } else {
                    conf->target_buf_size = value;
                }
                break;
            default:
                errored = 1;
        }

        if (optind < argc && *argv[optind] != '-') {
            conf->path = argv[optind];
            optind++;
        }
    }

    if (optind < argc && *argv[optind] != '-') {
        conf->path = argv[optind];
        optind++;
    }

    if (errored != 0 || conf->path == NULL) {
        return -1;
    }
    return 0;
}

void on_signal(int sig_num) {
    should_exit = 1;
}

int override_signal(int sig_num) {
    struct sigaction sa;
    memset(&sa, sizeof(struct sigaction), 0);
    sa.sa_handler = &on_signal;
    sa.sa_flags = SA_RESETHAND;

    return sigaction(sig_num, &sa, NULL);
}

void eprintf_override_signal(int sig_num, int err) {
    eprintf("Could not override signal %d (%s): error %d (%s)\n", sig_num, strsignal(sig_num), err, strerror(err));
}

int redirect_splice(int fd_in, int fd_out) {
    while (!should_exit) {
        ssize_t s_ret = splice(fd_in, NULL, fd_out, NULL, TRANSFER_LEN, SPLICE_F_MOVE);
        if (s_ret == -1) {
            if (errno == EINTR) {
                if (should_exit) {
                    // User called told us to exit.
                    return 0;
                }
                // Got interrupted by another signal, continue.
            } else {
                // Other error.
                return -1;
            }
        } else if (s_ret == 0) {
            // This should not happen.
            errno = EIO;
            return -1;
        }
    }
    return 0;
}

int redirect_copy(int fd_in, int fd_out) {
    char buf[TRANSFER_LEN];

    while (should_exit == 0) {
        ssize_t r_ret = read(fd_in, buf, TRANSFER_LEN);
        if (r_ret == -1 && errno != EINTR) {
            return -1;
        } else if (r_ret > 0) {
            // Write.
            char* buf_ptr = buf;
            char* buf_end = buf + r_ret;

            while (should_exit == 0 && buf_ptr < buf_end) {
                ssize_t w_ret = write(fd_out, buf_ptr, buf_end - buf_ptr);
                if (w_ret == -1 && errno != EINTR) {
                    return -1;
                } else if (w_ret > 0) {
                    buf_ptr += w_ret;
                }
            }
        }
    }
}

int redirect_forever(int fd_in, int fd_out) {
    int sp_ret = redirect_splice(fd_in, fd_out);
    if (sp_ret == -1 && errno == EINVAL) {
        /*
         * Could be the std* output file descriptor being bound to a file in a filesystem that does not support splice.
         * Fallback to the naive copying implementation.
         */
        return redirect_copy(fd_in, fd_out);
    }
    return sp_ret;
}

void print_usage(FILE* fd) {
    fprintf(fd, "Usage: %s [OPTION] FILE\n", program_invocation_short_name);
}

void print_help() {
    print_usage(stdout);
    printf(
        "Create a proxy file and redirect the writes to standard output\n"
        "\n"
        "  -e        Print to standard error\n"
        "  -o        Print to standard output (default)\n"
        "  -i        Do not redirect writes to the proxy file to the standard output,\n"
        "            but send the standard input to it instead\n"
        "  -b=SIZE   Set the buffer size. Kernel limits may still apply\n"
        "  -h        Print this help\n"
    );
}

/*
 * The readfile() syscall is too annoying to use.
 */
ssize_t readfile(const char* path, char* buf, size_t buf_size) {
    int o_ret = open_w(path, O_RDONLY);
    if (o_ret == -1) {
        return -1;
    }
    ssize_t r_ret = read_w(o_ret, buf, buf_size);
    close(o_ret);
    return r_ret;
}

int get_max_pipe_size() {
    char buf[MAX_PIPE_SIZE_MAX_FILE_LEN];
    memset(buf, MAX_PIPE_SIZE_MAX_FILE_LEN, '\0');

    ssize_t r_ret = readfile(MAX_PIPE_SIZE_SYS_PATH, buf, MAX_PIPE_SIZE_MAX_FILE_LEN - 1);
    if (r_ret < 1) {
        return -1;
    }

    int a_ret = atoi(buf);
    if (a_ret == 0) {
        errno = EIO;
        return -1;
    }

    return a_ret;
}

int resize_pipe(int pipe_fd, int target_size) {
    int ret = fcntl64(pipe_fd, F_SETPIPE_SZ, target_size);
    if (ret == -1 && errno == EPERM) {
        /*
         * Asked for more than we're allowed. Since we're asking beyond the limit,
         * do the next best thing and just ask for the limit.
         */
        int max_size = get_max_pipe_size();
        if (max_size == -1) {
            return -1;
        }
        return fcntl64(pipe_fd, F_SETPIPE_SZ, max_size);
    }
    return ret;
}

int open_pipe_fd(const char* path) {
    // Must include O_RDWR in read mode, since writer-less pipes are non-blocking when read.
    // Must inclide O_RDWR in write mode, since reader-less pipes error out with SIGPIPE/EPIPE.
    return open_w(path, O_RDWR);
}

int prepare_pipe(const char* path, mode_t mode) {
    while (1) {
        int mk_ret = mkfifo(path, mode);
        if (mk_ret == -1) {
            if (errno != EINTR) {
                // If the file already exists, it could be a pipe.
                if (errno == EEXIST) {
                    return 0;
                } else {
                    return -1;
                }
            }
        } else {
            return 0;
        }
    }
}

int is_pipe(int fd) {
    struct stat s;
    while (1) {
        int ret = fstat(fd, &s);
        if (ret == -1) {
            if (errno != EINTR) {
                return -1;
            }
        } else {
            if (S_ISFIFO(s.st_mode)) {
                return FD_IS_PIPE;
            }
            return FD_NOT_PIPE;
        }
    }
}

int main(int argc, char** argv) {
    struct conf c;
    int c_ret = get_conf(&c, argc, argv);
    if (c.help != 0) {
        print_help();
        return EX_OK;
    }
    if (c_ret == -1) {
        print_usage(stderr);
        return EX_USAGE;
    }

    // Prepare the input pipe.
    if (prepare_pipe(c.path, S_IRUSR | S_IWUSR | S_IRGRP)) {
        eprintf("Could not create pipe: error %d (%s)\n", errno, strerror(errno));
        return EX_NOINPUT;
    }

    int o_ret = open_pipe_fd(c.path);
    if (o_ret == -1) {
        eprintf("Could not open pipe: error %d (%s)\n", errno, strerror(errno));
        return EX_NOINPUT;
    }
    int fd_pipe = o_ret;

    int s_ret = is_pipe(fd_pipe);
    if (s_ret == FD_NOT_PIPE) {
        eprintf("File exists and is not a pipe: %s\n", c.path);
        return EX_NOINPUT;
    }
    if (s_ret != FD_IS_PIPE) {
        eprintf("Could not stat pipe: error %d (%s)\n", errno, strerror(errno));
        return EX_NOINPUT;
    }

    if (c.target_buf_size != BUF_SIZE_DEFAULT) {
        if (resize_pipe(fd_pipe, c.target_buf_size) == -1) {
            eprintf("Could not resize pipe: error %d (%s)\n", errno, strerror(errno));
            return EX_OSERR;
        }
    }

    // Override signals.
    should_exit = 0;
    if (override_signal(SIGTERM)) {
        eprintf_override_signal(SIGTERM, errno);
        return EX_OSERR;
    }
    if (override_signal(SIGINT)) {
        eprintf_override_signal(SIGINT, errno);
        return EX_OSERR;
    }
    if (override_signal(SIGHUP)) {
        eprintf_override_signal(SIGHUP, errno);
        return EX_OSERR;
    }

    // Loop redirect according to the mode.
    if (c.mode == OUTPUT_MODE) {
        if (redirect_forever(fd_pipe, STDOUT_FILENO) == -1) {
            eprintf("Could not redirect from pipe: error %d (%s)\n", errno, strerror(errno));
            unlink(c.path);
            close(fd_pipe);
            return EX_IOERR;
        }
    } else {
        if (redirect_forever(STDIN_FILENO, fd_pipe) == -1) {
            eprintf("Could not redirect to pipe: error %d (%s)\n", errno, strerror(errno));
            unlink(c.path);
            close(fd_pipe);
            return EX_IOERR;
        }
    }

    unlink(c.path);
    close(fd_pipe);
    return EX_OK;
}
