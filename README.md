# fproxy

Create a proxy file, redirecting writes to the standard output.

`fproxy` uses linux's `splice()` system call to achieve zero-copy redirection, falling back on a naive `read()`/`write()` copy implementation if `splice()` is not supported.

```sh
$ fproxy -h
Usage: fproxy [OPTION] FILE
Create a proxy file and redirect the writes to standard output

  -e        Print to standard error
  -o        Print to standard output (default)
  -i        Do not redirect writes to the proxy file to the standard output,
            but send the standard input to it instead
  -b=SIZE   Set the buffer size. Kernel limits may still apply
  -h        Print this help
```
